#!/usr/bin/env python
# -*- coding: utf-8 -*-

###  Homepage: http://www.paulojorgepm.net  ###

import os
import sys
import codecs
import Tkinter

f_text = 'Confirmar alterações dos seguintes nomes de ficheiros?\n'
STARTING_NUMBER = 1
#For all files in the folder change to "."
FILE_EXTENSION = ".jpg"
NAME_BEFORE = "album_holidays-"

def change_files(change=False):
    n=STARTING_NUMBER
    global f_text
    f_text += '\r\n'
    try:
        for root, dirs, files in os.walk('.'):
            for file in files:
                if file.endswith(FILE_EXTENSION): #colocar a extensão dos ficheiros que queremos mudar, p.e.: '.html' Ou deixar apenas '' se queremos mudar todos
                    new_name = NAME_BEFORE+str(n)+FILE_EXTENSION
                    if change == False:
                        f_text += file + ' -> ' + new_name + '\r\n'
                    elif change == True:
                        os.rename(file,new_name)
                        f_text += file + ' -> ' + new_name + ': Alterado com Sucesso!\r\n'
                    n=n+1       
    except:
        f_text = 'Error!\n'

change_files(change=False)
        
class ScrolledText(Tkinter.Frame):
    def __init__(self, parent=None, text=f_text, file=None):
        Tkinter.Frame.__init__(self, parent)
        self.pack(expand=Tkinter.YES, fill=Tkinter.BOTH)               
        self.makewidgets()
        self.settext(text, file)
    def makewidgets(self):
        sbar = Tkinter.Scrollbar(self)
        text = Tkinter.Text(self, relief=Tkinter.SUNKEN)
        sbar.config(command=text.yview)                  
        text.config(yscrollcommand=sbar.set)           
        sbar.pack(side=Tkinter.RIGHT, fill=Tkinter.Y)                   
        text.pack(side=Tkinter.LEFT, expand=Tkinter.YES, fill=Tkinter.BOTH)     
        self.text = text
    def settext(self, text='', file=None):
        if file: 
            text = open(file, 'r').read()
        self.text.delete('1.0', Tkinter.END)                   
        self.text.insert('1.0', text)                  
        self.text.mark_set(Tkinter.INSERT, '1.0')              
        self.text.focus()
        
class Window(ScrolledText):                        
    def __init__(self, parent=None, file=None): 
        frm = Tkinter.Frame(parent)
        frm.pack(fill=Tkinter.X)
        Tkinter.Button(frm, text='Confirmar',  command=self.onConfirmar).pack(side=Tkinter.LEFT)
        Tkinter.Button(frm, text='Cancelar/Sair',   command=self.onCancelar).pack(side=Tkinter.LEFT)
        ScrolledText.__init__(self, parent, file=file) 
        self.text.config(font=('courier', 9, 'normal'))
    def onConfirmar(self):
        change_files(change=True)
        self.text.delete('1.0', Tkinter.END) 
        self.text.insert('1.0', f_text)
    def onCancelar(self):
        self.destroy()

if __name__ == '__main__':
    try:
        Window().mainloop()   
    except IndexError:
        Window().mainloop()  