# Change name of all files in a folder

This script is a blueprint for automatically changing the names of all files with a certain extension (.jpg, .pdf, etc.), renaming them according to a logic (f.e. photo_1, photo_2, photo_3, etc.).
I use it for example for photos, when I have a ton of them from the cam and want them to have a login in the name (f.e. the date + a number).

To use it you need to edit the variables at start of the the code for the desired name  or extension search. At the moment it searchs for all files ending with ".html" and name them " new-" + number (new-1.html, new-2.html, new-3.html, etc.). Just put the "main.py" file a in a folder an run it: it runs relative to that folder.

The script uses Tkinker as GUI, and 1st asks for confirmation on the name changes and shows the final result before proceeding.

This was teste only with Python 2.7

##Contacts:
My Homepage: [www.paulojorgepm.net](http://www.paulojorgepm.net)